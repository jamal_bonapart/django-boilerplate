from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

from brdyorn.views import *
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
admin.autodiscover()

urlpatterns = patterns('',
    (r'^$', direct_to_template, {'template':'site/index.html'}, 'index'),
    
    url(r'^admin/', include(admin.site.urls)),
)
